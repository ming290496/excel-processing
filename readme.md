# Excel-Processing

This project is to compare excel processing capabilities of some certain excel libraies in .NET application.

These are the library I use:

- NPOI (Nuget Package Manager)
`
Install-Package NPOI -Version 2.4.1
`
- FreeSpire.xls (Nuget Package Manager)
`
Install-Package FreeSpire.XLS -Version 8.3.0
`
- Microsoft Office Interop Excel (Add Reference Extension)

## Screenshot
![](img/img_screenshot1.png)
![](img/img_screenshot2.png)
![](img/img_screenshot3.png)

## License
[SUMEDCODING.COM](http://sumedcoding.com/)