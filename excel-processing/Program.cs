﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading;

using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.Util;
using NPOI.SS.Util;

using Excel = Microsoft.Office.Interop.Excel;

using SpireLibrary = Spire.Xls;
using Spire.License;
using Spire.CompoundFile;

namespace excel_processing
{
    class Program
    {
        static void Main(string[] args)
        {
            int? input = null;
            int compareTime = 0;
            do
            {
                try
                {
                    Console.Write("How many rows do you want to create? ");
                    input = Convert.ToInt32(Console.ReadLine());
                }
                catch { };
            } while (input == null || input == 0);

            compareTime = (int)input;
            newLine(1);

            createNewExcel_excelInterop(compareTime);
            createNewExcel_spire(compareTime);
            createNewExcel_npoi(compareTime);

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
        static void createNewExcel_npoi(int row)
        {
            Console.WriteLine("Using NPOI Library :");
            DateTime start = DateTime.Now;
            XSSFWorkbook workbook = new XSSFWorkbook();
            ISheet worksheet = workbook.CreateSheet("First Sheet");

            for (int x = 1; x <= row; x++)
            {
                worksheet.CreateRow(x - 1).CreateCell(3).SetCellValue("Line - " + x);
                Console.Write("\rProcessing {0} rows...", x);
            }
            worksheet.AutoSizeColumn(3);

            newLine(1);
            string outputFile = "D:\\excelProcessing_NPOI - " + DateTime.Now.ToString("ddMMyyhhmmss") + ".xlsx";
            using (var fs = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
            {
                workbook.Write(fs);
            }
            workbook.Close();

            DateTime end = DateTime.Now;
            TimeSpan span = end - start;
            float ms = (float)span.TotalMilliseconds;
            Console.WriteLine("Timespan: " + (ms / 1000).ToString("0.000") + " seconds");
            Console.WriteLine("=====================================");
            newLine(2);
        }

        static void createNewExcel_excelInterop(int row)
        {
            Console.WriteLine("Using Excel Interop Library :");
            DateTime start = DateTime.Now;

            Excel.Application xlApp = new Excel.Application();
            xlApp.Visible = false;
            xlApp.DisplayAlerts = false;

            Excel.Workbook workbook = xlApp.Workbooks.Add(Type.Missing);
            Excel.Worksheet worksheet = workbook.ActiveSheet;
            worksheet.Name = "First Sheet";

            for (int x = 1; x <= row; x++)
            {
                worksheet.Cells[x, 4] = "Line - " + x;
                Console.Write("\rProcessing {0} rows...", x);
            }
            worksheet.UsedRange.Columns.AutoFit();

            newLine(1);
            string outputFile = "D:\\excelProcessing_ExcelInterop - " + DateTime.Now.ToString("ddMMyyhhmmss") + ".xlsx";

            workbook.SaveAs(outputFile);
            workbook.Close();

            DateTime end = DateTime.Now;
            TimeSpan span = end - start;
            float ms = (float)span.TotalMilliseconds;
            Console.WriteLine("Timespan: " + (ms / 1000).ToString("0.000") + " seconds");
            Console.WriteLine("=====================================");
            newLine(2);
        }

        static void createNewExcel_spire(int row)
        {
            Console.WriteLine("Using Spire Library :");
            DateTime start = DateTime.Now;

            SpireLibrary.Workbook workbook = new SpireLibrary.Workbook();
            workbook.Version = SpireLibrary.ExcelVersion.Version2013;
            SpireLibrary.Worksheet worksheet = workbook.Worksheets[0];
            worksheet.Name = "First Sheet";

            for (int x = 1; x <= row; x++)
            {
                worksheet.Range[x, 4].Text = "Line - " + x;
                Console.Write("\rProcessing {0} rows...", x);
            }
            worksheet.AllocatedRange.AutoFitColumns();

            newLine(1);
            string outputFile = "D:\\excelProcessing_Spire - " + DateTime.Now.ToString("ddMMyyhhmmss") + ".xlsx";

            workbook.SaveToFile(outputFile, SpireLibrary.ExcelVersion.Version2013);
            workbook.Dispose();

            DateTime end = DateTime.Now;
            TimeSpan span = end - start;
            float ms = (float)span.TotalMilliseconds;
            Console.WriteLine("Timespan: " + (ms / 1000).ToString("0.000") + " seconds");
            Console.WriteLine("=====================================");
            newLine(2);
        }

        static void newLine(int count)
        {
            for (int x = 0; x < count; x++)
            {
                Console.WriteLine();
            }
        }
    }
}
